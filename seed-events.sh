#!/bin/sh -e
HOST=mobile-dev-0.sofitest.com
email="wealth+987654321@java.sofi.org"
password="password1"

login() {
  echo "logging in"
  credentials="{\"email\": \"$email\", \"password\": \"$password\"}"
  contentTypeJson="Content-Type:application/json"
  (curl -X POST -v -c cookies.txt "https://$HOST/mobius/api/v1/auth/login" -H "$contentTypeJson" -d "$credentials")
}

createVenues() {
  echo "creating venues"
  venuefile="venues.txt"
  while IFS='' read -r line || [[ -n "$line" ]]; do
    curl -X POST \
    "https://$HOST/social-events/api/v1/events/venues" \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -b cookies.txt \
    -d "$line" | python -c "import sys, json; print json.load(sys.stdin)['id']" >> temp/venueIds.txt
  done < $venuefile
}

uploadImages(){
  find $PWD/images -type f | while read line; do
    curl -X POST \
	  "https://$HOST/social-events/api/v1/events/images" \
	  -H 'Cache-Control: no-cache' \
	  -H "Accept: application/json" \
	  -H 'content-type: multipart/form-data;' \
	  -b cookies.txt \
	  -F image="@$line" | python -c "import sys, json; print json.load(sys.stdin)['imageId']" >> temp/imageIds.txt
  done
}

createEvents(){
  echo "creating events"
  eventsFile="events.txt"
  i=0
  IFS=$'\r\n' GLOBIGNORE='*' command eval 'images=($(cat temp/imageIds.txt))'
  IFS=$'\r\n' GLOBIGNORE='*' command eval 'venues=($(cat temp/venueIds.txt))'
  while IFS='' read line || [[ -n $line ]]; do
  	IMAGE_ID=${images[i]}
  	VENUE_ID=${venues[i]}

  	#replace EVENT_IMAGE_ID with the id of the image we loaded
  	eventData=$(echo "$line" | sed "s/EVENT_IMAGE_ID/$IMAGE_ID/g;s/EVENT_VENUE_ID/$VENUE_ID/g")
	curl -X POST \
	  "https://$HOST/social-events/api/v1/events" \
	  -H 'Cache-Control: no-cache' \
	  -H 'Content-Type: application/json' \
	  -b cookies.txt \
	  -d "$eventData"
	  ((i++))
  done < $eventsFile
}

login
createVenues
uploadImages
createEvents
